package com.lessandro.transcol;

import java.io.*;
import java.util.*;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;

/**
 * Bigfield
 * @author lzm
 */
public class Bigfield extends MIDlet implements CommandListener {
    Vector linhas, uteis, sabado, domingo, atipicos, obs;
    InputStream is;

    Command info, next, back;
    List list;

    String[] dows = {"Dia Util", "Sabado", "Domingo", "Atipicos"};
    Vector[] dowv = {null, null, null, null};
    int now, dow;

    public Bigfield() {
        linhas = new Vector();
        dowv[0] = uteis = new Vector();
        dowv[1] = sabado = new Vector();
        dowv[2] = domingo = new Vector();
        dowv[3] = atipicos = new Vector();
        obs = new Vector();
        is = getClass().getResourceAsStream("/bigfield.txt");
        info = new Command("Info", Command.HELP, 1);
        next = new Command("Next", Command.BACK, 1);
        back = new Command("Back", Command.BACK, 1);
    }

    String readline() {
        String tmp = "";

        try {
            for (;;) {
                char c = (char)is.read();

                if (c == '\r') continue;
                if (c == '\n') break;
                //if ((c&0xe0) == 0xc0) {
                //    c = (char)((c&0x1f) << 6);
                //    c |= ((char)is.read())&0x3f;
                //}

                tmp += (char)c;
            }
        } catch (IOException ex) {
        }

        return tmp.trim();
    }

    String readblock() {
        String s = "";

        for (;;) {
            String tmp = readline();
            if (tmp.equals("")) break;
            s += tmp + "\n";
        }

        return s;
    }

    Vector tokenize(String s) {
        Vector tokens = new Vector();
        String tmp = "";

        for (int i=0; i<s.length(); i++) {
            char c = s.charAt(i);
            if (c <= ' ' || c == '-') {
                if (!tmp.equals("")) {
                    tokens.addElement(tmp);
                    tmp = "";
                }
            } else
                tmp += c;
        }

        return tokens;
    }

    void readdata() {
        for (;;) {
            String linha = readline();

            if (linha.equals("end"))
                break;

            readline();
            linhas.addElement(linha);
            uteis.addElement(tokenize(readblock()));
            sabado.addElement(tokenize(readblock()));
            domingo.addElement(tokenize(readblock()));
            atipicos.addElement(tokenize(readblock()));
            obs.addElement(readblock());
        }
    }

    int digit(String s, int i) {
        return s.charAt(i)-'0';
    }

    int parsedate(String date) {
        int hours = digit(date, 0)*10 + digit(date, 1);
        int minutes = digit(date, 3)*10 + digit(date, 4);

        return hours*60 + minutes;
    }

    void setuptime() {
        Calendar cal = Calendar.getInstance();

        now = cal.get(Calendar.HOUR_OF_DAY)*60 +
            cal.get(Calendar.MINUTE);

        dow = 0;
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) dow = 1;
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) dow = 2;
    }

    String gettimes(int i, int num, boolean prev) {
        String linha = (String)linhas.elementAt(i);
        Vector horas = (Vector)dowv[dow].elementAt(i);

        String ultimo = "", tmp = "";
        int n = 0;

        for (int j=0; j<horas.size(); j++) {
            String hora = (String)horas.elementAt(j);
            int time = parsedate(hora);

            if (time < now) {
                if (prev)
                    ultimo = hora;
                continue;
            }
            if (n == num) break;

            tmp += " "+hora;
            n++;
        }

        return linha+": "+ultimo+tmp;
    }

    void showstuff() {
        list = new List(dows[dow], List.IMPLICIT);
        Display.getDisplay(this).setCurrent(list);

        for (int i=0; i<linhas.size(); i++)
            list.append(gettimes(i, 3, false), null);

        list.setSelectCommand(info);
        list.addCommand(next);
        list.setCommandListener(this);
    }

    protected void startApp() {
        readdata();
        setuptime();
        showstuff();
    }

    void showinfo() {
        int i = list.getSelectedIndex();

        String tmp = gettimes(i, 10, true)+"\n\n"+(String)obs.elementAt(i);
        StringItem si = new StringItem(null, tmp.toUpperCase());

        si.setFont(Font.getFont(Font.FACE_MONOSPACE,
                Font.STYLE_PLAIN, Font.SIZE_MEDIUM));

        Form form = new Form("Info");
        form.append(si);

        form.addCommand(back);
        form.setCommandListener(this);

        Display.getDisplay(this).setCurrent(form);
    }

    void cycleday() {
        dow = (dow+1) % 3;
        showstuff();
    }

    public void commandAction(Command c, Displayable d) {
        if (c == info) showinfo();
        if (c == next) cycleday();
        if (c == back) Display.getDisplay(this).setCurrent(list);
    }

    protected void pauseApp() { }
    protected void destroyApp(boolean unconditional) { }
}
